import pygame
from pygame.locals import *

# catch all for anything that doesn't fit, or clutters up other files

def fitRange(center, size, containerSize):
    halfSize = size / 2
    origin = center - halfSize
    end = origin + size
    tooLow = origin < 0
    if tooLow:
        origin = 0
        end = origin + size
    if end >= containerSize: # tooHigh
        end = containerSize - 1
        if not tooLow: # didn't clip low, need to adjust
            origin = max((end - size), 0) # but it may clip after adjustment
    newCenter = (origin + end) / 2
    return (origin, newCenter, end)

def test_fitRange():
    print "Test fitRange()"
    print "  no adjust, even range"
    assert fitRange(50, 10, 100) == (45, 50, 55)
    print "  no adjust, odd range"
    assert fitRange(50, 9, 100) == (46, 50, 55)
    print "  low adjust, even range"
    assert fitRange(5, 20, 100) == (0, 10, 20)
    print "  low adjust, odd range"
    assert fitRange(5, 21, 100) == (0, 10, 21)
    print "  high adjust, even range"
    assert fitRange(95, 20, 100) == (79, 89, 99)
    print "  high adjust, odd range"
    assert fitRange(95, 21, 100) == (78, 88, 99)
    print "  crop, even box"
    assert fitRange(50, 150, 100) == (0, 49, 99)
    print "  crop, odd box"
    assert fitRange(50, 150, 101) == (0, 50, 100)
    print "  does end slip under the checks?"
    assert fitRange(0, 43, 30) == (0, 14, 29)

def cropRectForFocus(worldSize, viewSize, focusPos):
    oX, cX, eX = fitRange(focusPos[0], viewSize[0], worldSize[0])
    oY, cY, eY = fitRange(focusPos[1], viewSize[1], worldSize[1])
    viewRect = Rect(oX, oY, (eX - oX), (eY - oY))
    newFocus = (cX, cY)
    return (viewRect, newFocus)

def runtests():
    test_fitRange()

if __name__ == "__main__":
    runtests()
