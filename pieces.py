import ConfigParser as cpar, os

import maps

pieces = {}

def parsePieces():
    requiredStats = ["icon", "speed", "hits", "strength",
                     "cost", "firstcost", "terrain"]
    filename = os.path.join(os.path.dirname(__file__), "piece_definitions")
    parser = cpar.RawConfigParser()
    parser.read(filename)
    pieceTypes = parser.sections()
    for piece in pieceTypes:
        stats = parser.options(piece)
        for req in requiredStats:
            if req not in stats:
                break # skip over pieces that do not include required stats
        transport = maxRange = citycap = None
        if "transport" in stats:
            transport = parser.get(piece, "transport")
        if "range" in stats:
            maxRange = parser.getint(piece, "range")
        if "citycap" in stats:
            citycap = parser.getboolean(piece, "citycap")
        pieceClass = buildPiece(piece,
                                parser.get(piece, "icon"),
                                parser.getint(piece, "speed"),
                                parser.getint(piece, "hits"),
                                parser.getint(piece, "strength"),
                                parser.getint(piece, "cost"),
                                parser.getint(piece, "firstcost"),
                                parser.get(piece, "terrain"),
                                transport,
                                maxRange,
                                citycap)
        pieces[piece] = pieceClass

terrainSwitch = {"air":(maps.CELL_SEA, maps.CELL_LAND),
                 "land":(maps.CELL_LAND,),
                 "sea":(maps.CELL_SEA,),
                 "orbit":(maps.CELL_SEA, maps.CELL_LAND)}

def buildPiece(pieceName, icon, speed, hits, strength, cost, firstcost, terrain,
               transport=None, maxRange=None, citycap=False):
    class Piece:
        iconName = icon
        buildCost = cost
        firstBuildCost = firstcost
        maxHits = hits
        unitSpeed = speed
        unitStrength = strength
        transportableUnits = transport
        maximumRange = maxRange
        capturesCities = bool(citycap)
        usableTerrain = terrainSwitch[terrain]
        def __init__(self, owner, position):
            self.owner = owner
            self.hits = self.maxHits
            self.position = position
            self.cargo = []
            self.ship = None # containing ship if on board a transport
            self.orders = None
            self.orbitMove = None # if unit is sat, this controls movement
    Piece.__name__ = pieceName
    return Piece

class City:
    def __init__(self, position):
        self.owner = None
        self.position = position
        self.production = None # current production
        self.work = 0 # units of work complete in current task
