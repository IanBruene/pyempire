import pygame, os.path
from pygame.locals import *
import util, maps

terrainNames = {maps.CELL_LAND:"land.png",
                maps.CELL_SEA:"water.png",
                maps.CELL_UNKNOWN:"unexplored.png"}

images = {}
scaledImages = {}

imageRoot = os.path.join(os.path.dirname(__file__), "images")

pieces = {}

def getImage(imageName):
    # Currently requires extension, make extension agnostic later
    if imageName not in images.keys():
        path = os.path.join(imageRoot, imageName)
        image = pygame.image.load(path).convert()
        images[imageName] = image
    return images[imageName]

def getScaledImage(imageName, scaling):
    if (imageName, scaling) not in scaledImages.keys():
        image = getImage(imageName)
        size = image.get_size()
        newSize = (int(size[0] * scaling), int(size[1] * scaling))
        scaled = pygame.transform.smoothscale(image, newSize)
        scaledImages[imageName, scaling] = scaled
    return scaledImages[imageName, scaling]

def colorizeMask(mask, color):
    # expects a white on black mask, changes the white to color
    myMask = mask.copy()
    surf = pygame.Surface(myMask.get_size())
    surf.fill(color)
    myMask.set_colorkey(0xFFFFFF)
    surf.blit(myMask, (0, 0))
    return surf

def getPieceIcon(pieceName, tileSize, teamColor):
    defaultColor = 0xFFFFFF
    if pieceName not in pieces.keys():
        # need to load the native resolution, white mask version of the piece
        tile = getImage(pieceName)
        tempScale = {64: tile}
        pieces[pieceName] = {defaultColor: tempScale}
    if teamColor not in pieces[pieceName].keys():
        # need to make a native resolution version in the given teamColor
        mask = pieces[pieceName][defaultColor][64]
        colorized = colorizeMask(mask, teamColor)
        colorized.set_colorkey(0x000000)
        pieces[pieceName][teamColor] = {}
        pieces[pieceName][teamColor][64] = colorized
    if tileSize not in pieces[pieceName][teamColor].keys():
        # need to make a scaled version
        # SCALE ONLY ON POWERS OF TWO OR FACE THE MEMORY CONSEQUENCES
        native = pieces[pieceName][teamColor][64]
        scaled = pygame.transform.smoothscale(native, (tileSize, tileSize))
        pieces[pieceName][teamColor][tileSize] = scaled
    return pieces[pieceName][teamColor][tileSize]

def initWindow(size):
    pygame.init()
    scr = pygame.display.set_mode(size, DOUBLEBUF)
    return scr

def draw(screen, worldview):
    hudWidth = 200
    scrSize = screen.get_size()
    mapRect = Rect(0, 0, scrSize[0] - hudWidth, scrSize[1])
    mapSurf, mouseRects = drawMap(mapRect.size, (0, 0), worldview, 0.5)
    screen.blit(mapSurf, mapRect)
    drawHUD(screen, worldview)
    pygame.display.flip()
    return mouseRects

def drawMap(viewSize, focusPos, theMap, scale=1.0):
    mouseRects = [] # used for mouse detection, format: (mapPos, tileRect)
    tileSize = int(64 * scale)
    tileCounts = (viewSize[0] / tileSize, viewSize[1] / tileSize)
    cropRect, newFocus = util.cropRectForFocus(theMap.size,
                                               tileCounts,
                                               focusPos)
    mapSurf = pygame.Surface(viewSize)
    x = 0
    for col in range(cropRect.x, cropRect.right + 1):
        y = 0
        for row in range(cropRect.y, cropRect.bottom + 1):
            terrain, city, units = theMap.getCellData((col, row))
            tile = drawTile(terrain, city, units, tileSize)
            tileRect = Rect(x, y, tileSize, tileSize)
            mouseData = ((col, row), tileRect)
            mouseRects.append(mouseData)
            mapSurf.blit(tile, tileRect)
            y += tileSize
        x += tileSize
    return (mapSurf, mouseRects)

def drawTile(terrain, city, units, tileSize):
    scaleFactor = tileSize / 64.0
    tile = pygame.Surface((tileSize, tileSize))
    terImage = getScaledImage(terrainNames[terrain], scaleFactor)
    tile.blit(terImage, (0, 0))
    if city == None:
        numUnits = len(units)
        if numUnits > 0:
            unit = units[0]
            if numUnits > 1:
                asterisk = getScaledImage("asterisk.png", scaleFactor)
                asterisk.set_colorkey(0x000000)
                tile.blit(asterisk, (0, 0))
            c = unit.owner.color
            color = (c.r << 16) + (c.g << 8) + c.b
            icon = getPieceIcon(unit.iconName, tileSize, color)
            tile.blit(icon, (0, 0))
    else:
        cityIcon = getScaledImage("city.png", scaleFactor)
        cityIcon.set_colorkey(0x000000)
        tile.blit(cityIcon, (0, 0))
    return tile

def drawHUD(screen, worldview):
    pass
