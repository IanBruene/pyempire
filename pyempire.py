import time, pygame.locals as pyl
import interface, input_handling, util
import maps, players, pieces, core

pieces.parsePieces()

# Note: every file is 42% scaffolding, cruft is the rule for now

class Main: # this is a class for callback purposes
    def __init__(self):
        self.keepGoing = False
        self.window = interface.initWindow((1600, 900))
        self.handlers = {"quit":self.handleQuit,
                         "click":self.handleClick,
                         "mouseover":self.checkMouseover}
        self.binds = input_handling.Binder()
        self.initBinds()
        self.core = core.Empire((40, 25), 5, 70, 8)
        self.lastTileRects = None
        self.lastMouseover = None
    
    def addPiece(self, player, pieceType, position):
        temp = pieces.pieces[pieceType](self.player, position)
        self.units.append(temp)
        self.territory.map[position].addUnit(temp)
        
    def initBinds(self):
        self.binds.setBinding("quit", pyl.QUIT, pyl.K_ESCAPE)
        self.binds.setBinding("click", 1)
        self.binds.setBinding("mouseover", "mousemove")
    
    def handleClick(self, downP=None, pos=None, button=None):
        if downP:
            tile = self.getTileForPosition(pos)
            print "clicked", tile
    
    def checkMouseover(self, downP=None, pos=None, button=None):
        tile = self.getTileForPosition(pos)
        if tile != self.lastMouseover:
            print "new mouseover:", tile, "old:", self.lastMouseover
            self.lastMouseover = tile
        
    
    def getTileForPosition(self, pos):
        if self.lastTileRects == None:
            return None
        for tile in self.lastTileRects:
            if tile[1].collidepoint(pos):
                return tile[0]
    
    def handleQuit(self, downP=None, blah=None, bluh=None):
        print "Closing down"
        self.keepGoing = False
    
    def main(self):
        self.keepGoing = True
        while self.keepGoing:
            self.window.fill((32, 32, 32))
            input_handling.mungInput(self.binds, self.handlers)
            self.lastTileRects = interface.draw(self.window,
                                                self.core.filterlessMap)
            time.sleep(0.05)

if __name__ == "__main__":
    m = Main()
    m.main()
