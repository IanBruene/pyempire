import random

# Cell types
CELL_LAND = "+"
CELL_SEA = "."
CELL_UNKNOWN = " "

class Territory:
    def __init__(self, size):
        self.size = size
        self.map = {}
        self.initMap()
    
    def initMap(self):
        for column in range(self.size[0]):
            for row in range(self.size[1]):
                pos = (column, row)
                self.map[pos] = TerritoryCell(pos)
    
    def getCell(self, pos):
        cell = self.map[pos]
        return cell

class Map:
    """Map is connected to a Territory, which it uses as a data source.
Map is a view of the world filtered based on what a given player can see."""
    def __init__(self, parent, player=None):
        self.parent = parent
        self.player = player # accesses to parent are filtered by player
        self.size = parent.size
    
    def getCellData(self, pos):
        cell = self.parent.getCell(pos)
        if self.player != None:
            if cell.explored.get(self.player) != True:
                return (CELL_UNKNOWN, None, None)
            if cell.visible.get(self.player) != True:
                return (cell.terrain, None, None)
        return (cell.terrain, cell.city, cell.units)

class TerritoryCell:
    def __init__(self, position, terrain=CELL_UNKNOWN):
        self.terrain = terrain
        self.position = position
        self.city = None # the city, if any, at this position
        self.units = [] # any units on this cell
        self.explored = {} # per user bool, True if ever seen
        self.visible = {} # per user bool, True if currently visible
    
    def addUnit(self, unit):
        self.units.append(unit)
    
    def removeUnit(self, unit):
        num = self.units.counrt(unit)
        while num > 0:
            self.units.remove(unit)
            num -= 1
    
    def setVisible(self, player, status):
        if status == True:
            self.explored[player] = True
            self.visible[player] = True
        else:
            self.visible[player] = False
    
    def displayUnit(self):
        numUnits = len(self.units)
        if numUnits == 0:
            return None
        else:
            unit = self.units[0]
            multiP = numUnits > 1
            return (unit, multiP)
