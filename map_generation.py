import random, cmath
import maps, pieces

def printer(mapArray, mapSize):
    for j in range(mapSize[1]):
        for i in range(mapSize[0]):
            print (" %s" % mapArray[i, j]),
        print
    print

def gen(x, y):
    d = {}
    for i in range(x):
        for j in range(y):
            d[i, j] = 0
    return d

def smooth(theMap, mapSize, smoothness):
    old = theMap
    new = gen(mapSize[0], mapSize[1])
    offsets = [(-1, -1), (-1, 0), (-1, 1), (0, -1),
               (0, 0), (0, 1), (1, -1), (1, 0), (1, 1)]
    for z in range(smoothness):
        for i in range(mapSize[0]):
            for j in range(mapSize[1]):
                total = 0
                for offset in offsets:
                    x = offset[0] + i
                    x %= mapSize[0] # wraps around
                    y = offset[1] + j
                    if (y < 0) or (y >= mapSize[1]):
                        y = j
                    total += old[x, y]
                new[x, y] = total / 9
        temp = old
        old = new
        new = temp
    return old

def histogram(theMap, maxHeight):
    hist = [0] * maxHeight
    for key in theMap.keys():
        hist[theMap[key]] += 1
    return hist

def generateMap(mapSize, smoothness, waterRatio, numCities):
    MAX_HEIGHT = 999
    theMap = gen(mapSize[0], mapSize[1])
    # init map with random selection of heights
    for i in range(mapSize[0]):
        for j in range(mapSize[1]):
            theMap[i, j] = random.randint(0, MAX_HEIGHT)
    # smoothing
    heightMap = smooth(theMap, mapSize, smoothness)
    # build histogram
    hgram = histogram(heightMap, MAX_HEIGHT)
    # find water height
    waterHeight = MAX_HEIGHT
    total = 0
    mSize = mapSize[0] * mapSize[1]
    for i in range(len(hgram)):
        cell = hgram[i]
        total += cell
        if (((total * 100) / mSize) > waterRatio) and (total >= numCities):
            waterHeight = i
            break
    # mark land / sea
    territory = maps.Territory(mapSize)
    for i in range(mapSize[0]):
        for j in range(mapSize[1]):
            cell = territory.getCell((i, j))
            if heightMap[i, j] > waterHeight:
                cell.terrain = maps.CELL_LAND
            else:
                cell.terrain = maps.CELL_SEA
    return territory

def placeCities(territory, numCities, minDistance):
    availableLand = [] # list of valid positions for cities
    cities = []
    while len(cities) < numCities:
        while len(availableLand) == 0:
            availableLand, minDistance = regenLand(territory, cities,
                                                   minDistance)
        i = random.randint(0, len(availableLand) - 1)
        pos = availableLand[i]
        city = pieces.City(pos)
        cities.append(city)
        cell = territory.getCell(pos)
        cell.city = city
        availableLand = removeLand(availableLand, city, minDistance)
    return cities

def regenLand(territory, cityList, minDistance):
    available = []
    positions = territory.map.keys()
    for pos in positions:
        if territory.getCell(pos).terrain == maps.CELL_LAND:
            available.append(pos)
    if len(cityList) > 0: # don't decrement on first run
        minDistance -= 1
        assert minDistance >= 0 # TODO: handle this better
    for city in cityList:
        available = removeLand(available, city, minDistance)
    return available, minDistance

def removeLand(availableLand, city, minDistance):
    newLand = []
    for pos in availableLand:
        diff = complex(*city.position) - complex(*pos)
        distance = cmath.polar(diff)[0]
        if distance >= minDistance:
            newLand.append(pos)
    return newLand
