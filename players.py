import pygame
from pygame.locals import *

import maps

playerColors = [Color(0xFFFFFF), Color(0x000000),
              Color(0xFFFF00), Color(0xFF00FF), Color(0x00FFFF),
              Color(0xFF0000), Color(0x00FF00), Color(0x0000FF)]

class Player:
    def __init__(self, color, territory):
        self.color = color
        self.units = []
        self.cities = []
        self.worldMap = maps.Map(territory, self)
