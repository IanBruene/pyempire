
import random
import maps, pieces, players, map_generation as mapgen

class Empire:
    def __init__(self, worldSize, smoothness, waterRatio, numCities):
        self.size = worldSize
        self.territory = mapgen.generateMap(worldSize, smoothness,
                                            waterRatio, numCities)
        self.cities = mapgen.placeCities(self.territory, numCities, 10)
        self.filterlessMap = maps.Map(self.territory)
        self.players = []
        self.units = []
        self.currentPlayer = None
    
    def addPlayer(isCPU, playerColor):
        player = players.Player(playerColor, self.territory)
    
    def attackCity(attacker, city):
        attackPlayer = attacker.owner
        defendPlayer = city.owner
        success = random.randint(0, 1)
        if success:
            self.setCityOwner(city, attackPlayer)
            self.killUnit(attacker) # army "dispersed" to maintain order
            self.eventLog.cityDefended(city)
        else:
            self.killUnit(attacker)
            self.eventLog.cityCapture(city)
