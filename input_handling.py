import pygame
from pygame.locals import *

class Binder:
    def __init__(self):
        self.labels = {} # Labels can be bound to >1 key, each entry is a set
        self.keys = {} # Each key can only be bound to one label
    
    def bindingsForLabel(self, label):
        temp = self.labels.get(label)
        if temp == None: # no set, nothing ever bound to this label
            return None
        elif len(temp) == 0: # empty set, nothing currently bound
            return None
        else:
            return temp
    
    def bindingForKey(self, key):
        return self.keys.get(key)
    
    def setBinding(self, label, *keys):
        if len(keys) < 1: return
        for key in keys:
            self.keys[key] = label
            if not self.labels.has_key(label):
                self.labels[label] = set()
            self.labels[label].add(key)
    
    def removeBinding(self, label, key):
        if key in self.keys:
            del self.keys[key]
        if label in self.labels:
            self.labels[label].discard(key)

def mungInput(binder, funcTable):
    # all funcTable entries must have the format (downP, pos, button)
    # all are optional, default to None
    for event in pygame.event.get():
        if event.type == QUIT:
            bind = binder.bindingForKey(QUIT)
            funcTable[bind]()
        elif event.type in (KEYUP, KEYDOWN):
            bindKey = event.key
            bind = binder.bindingForKey(bindKey)
            if bind != None:
                downP = (event.type == KEYDOWN)
                funcTable[bind](downP, None, None)
        elif event.type in (MOUSEBUTTONUP, MOUSEBUTTONDOWN):
            bindKey = event.button
            bind = binder.bindingForKey(bindKey)
            if bind != None:
                downP = (event.type == MOUSEBUTTONDOWN)
                funcTable[bind](downP, event.pos, event.button)
        elif event.type == MOUSEMOTION:
            bind = binder.bindingForKey("mousemove")
            if bind != None:
                funcTable[bind](None, event.pos, None)
